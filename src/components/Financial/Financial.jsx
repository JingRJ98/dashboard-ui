import CardTop from '../CardTop';

const Financial = () => {
  return (
    <div className="subgrid-two-item grid-common grid-c8">
      <CardTop name="Financial Advice" />
      <div className="grid-c8-content">
        <p className="text text-silver-v1">Ipsum dolor sit amet consectetur, adipisicing elit.
          Iste, vitae.....</p>
      </div>
    </div>
  )
}

export default Financial
