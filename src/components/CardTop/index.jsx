import { iconsImgs } from "../../utils/images";
import PropTypes from 'prop-types'

const CardTop = ({ name }) => {
  return (
    <div className="grid-c-title">
      <h3 className="grid-c-title-text">{name}</h3>
      <button className="grid-c-title-icon">
        <img src={iconsImgs.plus} />
      </button>
    </div>
  )
};
export default CardTop;

CardTop.propTypes = {
  name: PropTypes.string
}