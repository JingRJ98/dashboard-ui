import "./Transactions.css";
import { transactions } from "../../data/data";
import CardTop from '../CardTop';

const Transactions = () => {
  return (
    <div className="grid-one-item grid-common grid-c2">
      <CardTop name="All Transactions" />
      <div className="grid-content">
        <div className="grid-items">
          {
            transactions.map(({ id, image, name, date, amount }) => (
              <div className="grid-item" key={id}>
                <div className="grid-item-l">
                  <div className="avatar img-fit-cover">
                    <img src={image} alt="" />
                  </div>
                  <p className="text">{name}
                    <span>{date}</span>
                  </p>
                </div>
                <div className="grid-item-r">
                  <span className="text-scarlet">$ {amount}</span>
                </div>
              </div>
            ))
          }
        </div>
      </div>
    </div>
  )
}

export default Transactions
